package com.example.irricontrolofflinenotificationapp
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.telephony.SmsMessage
import android.util.Log
import androidx.core.content.ContextCompat.startActivity
import android.R.id.edit
import android.content.SharedPreferences.*
import android.content.SharedPreferences
import android.widget.ArrayAdapter
import android.widget.ListView
import java.io.IOException
import java.lang.Exception


class MyReceiver : BroadcastReceiver() {

    private val SMS_RECEIVED: String = "android.provider.Telephony.SMS_RECEIVED"
    private val TAG: String = "SmsBroadcastReceiver"
    lateinit var msg: String
    var flag: Boolean = false

    val CHANNEL_ID: String = "personal_notifications"
    private val NOTIFICATION_ID: Int = 1

    override fun onReceive(context: Context, intent: Intent) {
        //retrieves the general action to be performed and display on log
        Log.i(TAG, "Intent Received: " + intent.action)
        if (intent.action == SMS_RECEIVED){

            //retrieves a map of extended data from the intent
            var dataBundle: Bundle? = intent.extras
            if (dataBundle != null){

                //creating PDU(Protocol Data Unity) object witch is a protocol for transferring message
                var mypdu = dataBundle.get("pdus") as Array<Any>
                //arrayOfNulls???
                val message = arrayOfNulls<SmsMessage>(mypdu.size)

                for (i in 0 until mypdu.size) {

                    //for build versions >= API Level 23
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        var format: String? = dataBundle.getString("format")

                        //From PDU we get all object and SmsMessage Objet using following line of code
                        message[i] = SmsMessage.createFromPdu(mypdu[i] as ByteArray, format)
                    } else {

                        //<API Level 23
                        message[i] = SmsMessage.createFromPdu(mypdu[i] as ByteArray)
                    }
                    msg = message[i]!!.messageBody
                    if (msg.contains("ERROR")) flag = true
                    //phoneNo = message[i]!!.originatingAddress!!
                }

                if (flag){

                    val intent = Intent(context, MainActivity::class.java)
                    intent.putExtra("pivos", msg)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    context.startActivity(intent)

                    //redirect to the Alert Dialog
                    val alarmIntent = Intent("android.intent.action.MAIN")
                    alarmIntent.setClass(context, AlertActivity::class.java!!)
                    alarmIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    context.startActivity(alarmIntent)
                }
            }
        }
    }

}
