package com.example.irricontrolofflinenotificationapp

import android.app.ActivityManager
import android.content.Context
import android.media.AudioManager
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.os.VibrationEffect
import android.os.Build
import android.os.Vibrator
import android.content.Intent
import android.util.Log
import android.widget.Toast




class AlertActivity : AppCompatActivity() {

    lateinit var v: Vibrator
    lateinit var mMediaPlayer: MediaPlayer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_alert)

        val win = window
        win.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED or WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD)
        win.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON or WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON)

        playSound()
        vibrate()
        animate()
    }

    fun okButton (view: View){
        /*val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        this.startActivity(intent)*/
        mMediaPlayer.stop()
        v.cancel()
        finish();
    }

    fun playSound (){
        mMediaPlayer = MediaPlayer.create(this, R.raw.analog_watch_alarm)
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC)
        mMediaPlayer.setLooping(true)
        mMediaPlayer.start()
    }

    fun vibrate (){
        var pattern = longArrayOf(0, 100, 1000)
        v = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        // Vibrate for 500 milliseconds
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(2000, VibrationEffect.DEFAULT_AMPLITUDE))
        } else {
            //deprecated in API 26
            v.vibrate(pattern, 0)
        }
    }

    fun animate (){
        val shake: Animation
        shake = AnimationUtils.loadAnimation(applicationContext, R.anim.shake)

        val image: ImageView
        image = findViewById(R.id.alert_img_view) as ImageView

        image.startAnimation(shake)
    }
}
