package com.example.irricontrolofflinenotificationapp

import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

class MainActivity : AppCompatActivity() {

    //ask for permission code
    val MY_PERMISSIONS_REQUEST_RECEIVE_SMS: Int = 0

    var pivoList = ArrayList<String>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var sharedPrefs: SharedPreferences = this.getSharedPreferences("PIVO_LIST", Context.MODE_PRIVATE)


        //check if permission is not granted
        var context: Context
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED){
            //if the permission is not granted then check if the user has denied the permission
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.RECEIVE_SMS)){

                //do nothing as user has denied

            } else {
                //a pop up will appear asking required permission i.e Allow or Deny
                ActivityCompat.requestPermissions(this, arrayOf<String>(android.Manifest.permission.RECEIVE_SMS), MY_PERMISSIONS_REQUEST_RECEIVE_SMS)
            }
        }

        var pivo = getIntent().getStringExtra("pivos")
        var str = sharedPrefs.getString("pivos", "")
        str = str + "\n" + pivo
        sharedPrefs.edit().putString("pivos", str).apply()

        println("PEGANDO SHARED PREFS: " + sharedPrefs.getString("pivos", ""))
        //PRONTO AGORA TENHO AS MENSAGENS SENDO ARMAZENADAS NO SHARED PREFS ACUMULANDO UMA POR UMA EM UMA STRING
        //FALTA EU CRIAR UMA FUNÇÃO PARA DESSERIALIZAR ESSA STRING EM UM ARRAYLIST<STRING> E MOSTRAR NA LISTVIEW

        //PRECISO DE UM DESSERIALIZADOR E SERIALIZADOR PERSONALIZADO! TEM QUE TIRAR OS VALORES NULL

        //Finally we can add the PivoList to our ListView

        //LEMBRANDO QUE AQUI FIZEMOS UMA GAMBIARRA PRA ELE MOSTRAR A STRING PELA ARRAYLIST
        var array = mutableListOf<String>()
        array.add(sharedPrefs.getString("pivos", "")!!)
       var listView: ListView = findViewById(R.id.pivoList) as ListView
        val adapter = ArrayAdapter<String>(this,
            android.R.layout.simple_list_item_1,
            array)
        listView.setAdapter(adapter)
        //println("TODOS OS PIVOS"+)

    }//onCreate
    // after getting the result of permission requests the result will be passed throught this method
    override fun onRequestPermissionsResult( requestCode:Int, permissions:Array<String>, grantResults:IntArray) {

        when (requestCode) {
            MY_PERMISSIONS_REQUEST_RECEIVE_SMS -> {
                //check weather the length of grantResults if greater than 0 and is equal to PERMISSIONS_GRANTED
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //***************** Now broadcastreceiver will work in background
                    Toast.makeText(this, "Permissão concecida", Toast.LENGTH_LONG)
                        .show()
                } else {
                    Toast.makeText(
                        this,
                        "Ação bloqueda, permissão não concedida pelo usuário.",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
    }
}
